const { article, blog, categorie } = require('../controllers/blog.controller.js');
const express = require('express');
const router = express.Router();



router.get('/', blog);
router.get('/:categorie', categorie);
router.get('/article/:id', article);



module.exports = router;