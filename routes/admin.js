const express = require('express');
const router = express.Router();
const adminController = require('../controllers/adminController.js');
const auth = require("../Middlewares/auth");



/* 
router.get('/', (req, res, next) => { res.redirect("/admin/login"); });
 */

/* LOGIN MANAGER */
router.get('/', function(req, res, next) { res.redirect("/admin/login") });
router.get('/login', adminController.login);
router.get('/logout', adminController.logout);
router.get('/register', adminController.register);
router.post('/register', adminController.registerAdmin); 
router.post('/login', adminController.loginAdmin); 

 /* CONTENTS MANAGER */
router.get('/contents', auth, adminController.getContents);
router.get('/contents/:page', auth, adminController.getContentsPage);
router.get('/contents/edit/:id', auth, adminController.getContentsForm);
router.post('/contents/edit/:id', auth, adminController.editContent);

/* ARTICLES MANAGER */
router.get('/articles', auth, adminController.getArticles);
router.get('/articles/edit/:id', auth, adminController.getArticlesForm);
router.get('/articles/add', auth, adminController.getAddArticle);
router.post('/articles/add', auth, adminController.addArticle);
router.post('/articles/edit/:id', auth, adminController.editArticle);
router.get('/articles/delete/:id', auth, adminController.deleteArticle);



module.exports = router;