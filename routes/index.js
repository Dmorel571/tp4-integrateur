var express = require('express');
var router = express.Router();
var contactsModel = require("../models/contactModel")
var bodyParser = require('body-parser');
var Controller = require("../controllers/Controller");
const app = require('../app');


/* Get Homepage */
router.get('/', Controller.index);
router.get('/index', Controller.index);
router.get('/contact', Controller.contact);

router.post("/contact", function(req, res, next) {
  let contact_model = new contactsModel(req.body);
  contact_model.save().then(contact_model =>{
    res.render("contacts", {contacts: contact_model});
  })
});


/* GET marketing page. */
router.get('/marketing', Controller.marketing);

/* GET A propos page. */
router.get('/apropos', Controller.apropos);



module.exports = router

