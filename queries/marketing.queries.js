const servicesModel = require('../models/services.model.js');

exports.getAllServices = () => {
    return servicesModel
        .find({})
        .sort({ order: 0 })
        .exec();
}