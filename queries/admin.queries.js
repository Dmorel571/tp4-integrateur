const loginModel = require('../models/login.model.js');

exports.getUser = (username) => {
    return loginModel.findOne({username: username}).populate("order").exec();
}