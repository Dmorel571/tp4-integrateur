const articlesModel = require('../models/articles.model.js');

exports.getArticlesByCategorie = (categorie) => {
    return articlesModel.find({categorie: categorie}).exec();
}

exports.getArticleById = (id) => { 
    return articlesModel.findById(id);
}

exports.getAllArticles = () => {
     return articlesModel.find({}).exec();
}

exports.addArticle = (data) => {
    articlesModel.create({ title: data.title, author: data.author, post_date: Date.now(), categorie: data.categorie, body: data.body })
}

exports.updateArticleById = (id, data) => {
    console.log(data);
    articlesModel.findOneAndUpdate( { _id: id }, { title: data.title, author: data.author, categorie: data.categorie, body: data.body }).exec();
}

exports.deleteArticle = (id) => {
    return articlesModel.findByIdAndRemove({_id: id});
}