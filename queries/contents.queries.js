const contentsModel = require("../models/contents.model.js");

exports.getContentsAndOtherByPage = async (page) => {
    const response = await contentsModel.find({page: {$in: ['other', page]}}).exec();
    return await sortArrayById(response);
}

exports.getContentsByPage = (page) => {
    return contentsModel.find({page: page}).exec();
}

exports.getOneContentById = (id) => {
    return contentsModel.findOne({_id: id}).exec();
}

exports.updateContentById = (id, body) => {
    return contentsModel.findOneAndUpdate( { _id: id }, { body: body });
}

function sortArrayById(data) {
    let table = {};
    for (const key in data) { table[data[key].id] = data[key]; }
    return table;
}