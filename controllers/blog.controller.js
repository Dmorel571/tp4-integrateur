const { getAllArticles, getArticleById, getArticlesByCategorie } = require("../queries/blog.queries.js");
const { getContentsAndOtherByPage } = require("../queries/contents.queries.js");

module.exports = {
    
    blog: async (req, res, next) => {
        try{
            //Récupération des textes de la page  
            const pageContents = await getContentsAndOtherByPage("blog");

            //Récupération des articles de la page
            const articlesData = await getAllArticles();
            for(var k in articlesData) { 
                let date = new Date(articlesData[k].post_date);
                articlesData[k].date = `${date.getDay()}/${date.getMonth()}/${date.getFullYear()}`;
                articlesData[k].body = articlesData[k].body.substring(0, 200)+"...";
            }

            //Date

            res.render('blog', { layout: false, articles: articlesData, c: pageContents });
        }catch(err){ console.log(err); res.sendStatus(500); }
    },

    categorie: async (req, res, next) => {
        try{
            //Récupération des textes de la page
            const pageContents = await getContentsAndOtherByPage("blog");
            
            //Récupération des articles de la page
            const articlesData = await getArticlesByCategorie(req.params.categorie);
            for(var k in articlesData) { articlesData[k].body = articlesData[k].body.substring(0, 200)+"..."; }
            console.log(articlesData);
            res.render('blog', { layout: false, articles: articlesData, c: pageContents });
        }catch(err){ console.log(err); res.sendStatus(500); }
    },
    
    article: async (req, res, next) => {
        try{
            //Récupération du text de la page
            const pageContents = await getContentsAndOtherByPage("blog");

            //Récupération du text de la page
            const articleData = await getArticleById(req.params.id);
            res.render('article', { layout: false, article: articleData, c: pageContents })
        }catch(err){ console.log(err); res.sendStatus(500); }
    }

}