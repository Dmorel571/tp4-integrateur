let {adminModel} = require('../models/adminModel');
const bcrypt = require ('bcrypt');
const {validationResult} = require('express-validator');
const contentsQueries = require('../queries/contents.queries');
const articlesQueries = require('../queries/blog.queries');
const articlesModel = require('../models/articles.model');


/* AUTH MANAGER */
exports.login = async (req,res,next) =>{
    if(req.session.authenticated) { res.redirect('/admin/contents'); }
    res.render('admin/login');
}  

exports.register = async (req,res,next) =>{
    if(req.session.authenticated) { res.redirect('/admin/contents'); }
    res.render('admin/register');
}  


exports.logout = async (req, res, next) => {
    if (req.session) { req.session.destroy(); }
    res.redirect('/admin/login');
}





/* CONTENTS MANAGER */
exports.getContents = async (req, res, next) => {
    res.render('admin/manager/manager', { typeContents: true });
}

exports.getContentsPage = async (req, res, next) => {
    try{
        const pagesData = await contentsQueries.getContentsByPage(req.params.page);
        res.render('admin/manager/manager', { typePage: true, data: pagesData });
    }catch (err) { serverError(err); }
}

exports.getContentsForm = async (req, res, next) => {
    try{
        const contentData = await contentsQueries.getOneContentById(req.params.id);
        res.render('admin/manager/manager_form', { typeEditContent: true, data: contentData });
    }catch (err) { serverError(err); }
}

exports.editContent = async (req, res, next) => {
    try{
        await contentsQueries.updateContentById(req.params.id, req.body.body);
        res.redirect("/admin/contents");
    }catch (err) { serverError(err); }
}







/* ARTICLES MANAGER */
exports.getArticles = async (req, res, next) => {
    try{
        const articlesData = await articlesQueries.getAllArticles();
        res.render('admin/manager/manager', { typeArticles: true, data: articlesData } ); 
    }catch (err) { serverError(err); }  
}

exports.getArticlesForm = async (req, res, next) => {
    try{
        const contentData = await articlesQueries.getArticleById(req.params.id);
        res.render('admin/manager/manager_form', {typeEditArticle: true, data: contentData });
    }catch(err) { serverError(err); }
}

exports.editArticle = async (req,res,next) => {
    try {
        await articlesQueries.updateArticleById(req.params.id, req.body);
        res.redirect('/admin/articles');
    } catch (err) { serverError(err); }
}

exports.getAddArticle = async (req,res,next) => {
    res.render('admin/manager/manager_form', { typeAddArticle: true });
} 

exports.addArticle = async (req,res,next) => {
    try {
        await articlesQueries.addArticle(req.body);
    } catch (err) { serverError(err); }
    res.redirect('/admin/articles');
} 

/* SUPPRIMER ARTICLE */
exports.deleteArticle = async (req,res,next) => {
    try {
        await articlesQueries.deleteArticle(req.params.id);
        res.redirect("/admin/articles");
    } catch (err) { serverError(err); }
}



/* SECTION INSCRIPTION ET CONNEXION */

/* REGISTER ADMIN */
exports.registerAdmin = async (req,res,next) =>{
    const errors = [];
    if (errors.length <= 0){
        try {
           
            const hashed_password = await bcrypt.hash(req.body.password, 10);
           
            let admin = new adminModel({
                username: req.body.name,
                password: hashed_password
            });

            admin.save((err, document) =>{
                if (err) res.render('register', {error:err});
                res.render('admin/login', {data:document});
            });
        
        } catch (err) { serverError(err); }
    }
}


/* LOGIN ADMIN */
exports.loginAdmin = async (req,res,next) =>{
    try {
        let admin = await adminModel.findOne({username:req.body.name});
        if(admin){
            let compare = await bcrypt.compare(req.body.password, admin.password);
            if(compare){
                req.session.authenticated = true;
                res.redirect("/admin/contents");
            } else {
                res.render('error', {message:"Mauvais nom d'utilisateur ou mot de passe."});
            }
        } else {
            res.render('error', {message: "Mauvais nom d'utilisateur ou mot de passe."});
        }
    }catch (err) { serverError(err); }
}


function serverError(err) {
    console.log(err);
    res.status(500).send("Internal Server error Occured");
}

