const { getAllServices } = require("../queries/marketing.queries.js");
const { getContentsAndOtherByPage } = require("../queries/contents.queries.js");


exports.index = async function(req,res,next){
    const pageContents = await getContentsAndOtherByPage("home");
    res.render('index', { layout: false , c: pageContents });
}

exports.marketing = async function(req,res,next){
    const pageContents = await getContentsAndOtherByPage("marketing");
    res.render('marketing', { layout: false, c: pageContents });
}

exports.apropos = async function(req,res,next){
    const pageContents = await getContentsAndOtherByPage("about");
    res.render('apropos', { layout: false, c: pageContents });
}

exports.contact = async function(req,res,next){
    const pageContents = await getContentsAndOtherByPage("contact");
    res.render('contact', { layout: false, c: pageContents });
}

exports.addcontact = async (req,res,next) => {
    let contact_model = new contactsModel(req.body);
    contact_model.save().then(contact_model =>{
        res.render("contacts", {contacts: contact_model});
    })
};
