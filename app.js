var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
const hbs = require('hbs');
const exSession = require('express-session');


require('dotenv').config(); 


/* VALIDATION */
const validation = require('./Middlewares/validationMiddleware');
const contactSchema = require('./Validations/contactValidation');



/* CONNEXION MONGOOSE */
mongoose.connect(
  "mongodb://localhost:27017/orange",{
  useNewUrlParser: true,
  useUnifiedTopology: true
  }
);


const db = mongoose.connection;
db.on("error", console.error.bind(console, "erreur de connection"));


var indexRouter = require('./routes/index');
var blogRouter = require('./routes/blog');
var adminRouter = require('./routes/admin');
const { validate } = require('./models/contactModel');





var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.json());
app.use(exSession({
  secret: process.env.SESSION_TOKEN,
  resave:false,
  saveUninitialized:false
})); 

/* USE ROUTER */
app.use('/', indexRouter);
app.use('/blog', blogRouter);
app.use('/admin', adminRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

/* YUP POST VALIDATION */
app.post("/contact", validation(contactSchema), (req,res) =>{
  res.status(200).send(req.body);
});


module.exports = app;


