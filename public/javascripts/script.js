function validateForm() {
    let inputNom =
        document.forms["RegForm"]["inputNom"];
    let inputPrenom =
        document.forms["RegForm"]["inputPrenom"];
    let inputEmail =
        document.forms["RegForm"]["inputEmail"];
    let inputPassword =
        document.forms["RegForm"]["inputPassword"];

    if (inputNom.value == '') {
        window.alert('Veuillez entrer votre nom.')
        inputNom.focus()
        return false
    }
    if (inputPrenom.value == '') {
        window.alert('Veuillez entrer votre prénom.')
        inputPrenom.focus()
        return false
    }

    if (inputEmail.value == '') {
        window.alert(
            'Veuillez entrer un courriel valide.')
        inputEmail.focus()
        return false
    }

    if (inputPassword.value == '') {
        window.alert(
            'Veuillez entrer votre mot de passe.')
        inputPassword.focus()
        return false
    }
    return true
}

$(document).ready(function() {
    $("#scroll-to-top").click(function() {
        $("html, body").animate({
            scrollTop: 0
        }, "slow");
        return false;
    });
}); 
