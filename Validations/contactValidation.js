 const yup = require('yup');


const contactSchema = yup.object({
    Prénom: yup.string().required(),
    Nom: yup.string().required(),
    Courriel: yup.string().email("Vous avez besoin du email").required(),
    Motdepasse: yup.string().min(4, "Il vous faut plus que 4 mots!").max(10, "Il vous faut moins que 10 mots!").required()
}, {
    timestamps:true
});





module.exports = contactSchema 