const {src, dest,watch} = require('gulp');
const sass = require('gulp-sass');
const concat = require("gulp-concat");


function compile_sass(){
    return src("./assets/scss/**/*.scss")
        .pipe(sass())
        .pipe(dest("./public/stylesheets/"));
}

function compile_js(){
    return src(['./assets/js/*.js'])
        .pipe(concat('script.js'))
        .pipe(dest('./public/javascripts'));
}

exports.default = function() {
    watch("./assets/scss/**/*.scss", compile_sass);
    watch('./assets/js/*.js', compile_js)
}