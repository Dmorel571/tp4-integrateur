const mongoose = require('mongoose');
const schema = mongoose.Schema;

const contentsSchema = schema({
    _id: { type: mongoose.Schema.Types.ObjectId, required: true },
    page: { type: String, required: true },
    section_name: { type: String, required: true },
    body: { type: String, required: true }
});

module.exports = mongoose.model("contents", contentsSchema);


