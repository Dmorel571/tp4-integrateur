const mongoose = require('mongoose');
const schema = mongoose.Schema;

const admin = new schema({
    username:{
        required:true,
        type:String
    },
    password:{
        required:true,
        type:String
    }
});

exports.adminModel = mongoose.model("admin", admin);