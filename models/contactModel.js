const mongoose = require('mongoose');

const Schema = mongoose.Schema;

let contactSchema = new Schema({
    Prénom: String,
    Nom: String,
    Courriel: {
        type:String,
        required: true,
        unique : true
        },
    Motdepasse: String
}, {
    timestamps:true
});

module.exports = mongoose.model("contact", contactSchema);

