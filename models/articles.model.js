const mongoose = require('mongoose');
const schema = mongoose.Schema;

const articlesSchema = schema({
    title: { type: String, required: true },
    author: { type: String, required: true },
    categorie: { type: String, required: true },
    post_date: { type: Number, required: true },
    body: { type: String, required: true }
});

module.exports = mongoose.model("articles", articlesSchema);


