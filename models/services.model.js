const mongoose = require('mongoose');
const schema = mongoose.Schema;

const servicesSchema = schema({
    _id: { type: mongoose.Schema.Types.ObjectId, required: true },
    title: { type: String, required: true },
    picture_url: { type: String, required: true },
    body: { type: String, required: true },
    order: { type: Number, required: true }
});

module.exports = mongoose.model("services", servicesSchema);